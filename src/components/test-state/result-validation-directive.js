function resultValidationDirective() {
    return {
        require: 'ngModel',
        link: (scope, element, attrs, ngModelCtrl) => {
            function myValidation(value) {
                if (value <= 100 && value >= 0) {
                    ngModelCtrl.$setValidity('integer', true);
                } else {
                    ngModelCtrl.$setValidity('integer', false);
                }
                return value;
            }
            ngModelCtrl.$parsers.push(myValidation);
        }
    };
}

export default[
    resultValidationDirective
];

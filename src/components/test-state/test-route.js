import template from './test-state.html!text';

function testRouteConfig($stateProvider) {
    $stateProvider
        .state('app.test', {
            url: 'test',
            views: {
                application: {
                    controller: 'TestStateController as testState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    testRouteConfig
];

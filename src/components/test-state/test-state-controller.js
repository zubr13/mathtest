class TestStateController {
    constructor(dataService){
        this.dataService = dataService;
        this.dataService.currentMode = 'test';
        this.currentExpression = 'Go please to setup settings and choose type of expressions';
        this.operations = [];
        this.disable = true;
        if(Object.keys(dataService.modes).length !== 0){
            this.disable = false;
            this.generateOperations();
            this.generateExpression();
        }
        this.rightAnswers = 0;
        this.wrongAnswers = 0;
    }

    checkAnswer(){
        if(this.answer === +this.userAnswer){
            this.rightAnswers++;
        }
        else{
            this.wrongAnswers++;
        }
        this.generateExpression();
    }

    generateExpression(){
        const operation = this.operations[this.randomInteger(0, this.operations.length - 1)];
        const x = this.randomInteger(0, 99);
        const y = this.randomInteger(0, 99);
        const expression = ` ${x} ${operation} ${y}`;
        if(eval(expression) <= 100 && eval(expression) % 1 === 0 && eval(expression) >= 0){
            this.currentExpression = `${expression} = ?`;
            this.answer = eval(expression);
        }
        else{
            this.generateExpression();
        }
    }

    randomInteger(min, max) {
        let rand = min - 0.5 + Math.random() * (max - min + 1)
        rand = Math.round(rand);
        return rand;
    }

    generateOperations(){
        const modes = this.dataService.modes;
        for(let key in modes){
            if(modes[key]){
                this.operations.push(key);
            }
        }
    }
}

export default [
    'dataService',
    TestStateController
];

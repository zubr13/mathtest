import './test-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import TestStateController from './test-state-controller';
import testRouteConfig from './test-route';
import resultValidationDirective from './result-validation-directive';

const dependencies = [
    'ui.router'
];

export default angular
    .module('test-state-component', dependencies)
    .controller('TestStateController', TestStateController)
    .directive('resultValidationDirective', resultValidationDirective)
    .config(testRouteConfig);

/**
 * Provides displaying UI notifications by toasts
 */
class DataService {
    constructor() {
        this.modes = {};
        this.currentMode = '';
    }
}

export default [
    DataService
];

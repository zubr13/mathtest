class ApplicationController {
    constructor($state, dataService){
        this.$state = $state;
        this.dataService = dataService;
    }

    chooseSetupMode(){
        this.$state.go('app.setup-settings');
        this.currentMode = this.dataService.currentMode;
    }

    chooseCheckMode(){
        this.$state.go('app.test');
        this.currentMode = this.dataService.currentMode;
    }
}

export default [
    '$state',
    'dataService',
    ApplicationController
];

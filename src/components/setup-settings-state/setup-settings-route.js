import template from './setup-settings-state.html!text';

function setupSettingsRouteConfig($stateProvider) {
    $stateProvider
        .state('app.setup-settings', {
            url: 'setup-settings',
            views: {
                application: {
                    controller: 'SetupSettingsStateController as setupSettingsState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    setupSettingsRouteConfig
];

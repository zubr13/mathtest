class SetupSettingsStateController {
    constructor($state, $scope, dataService){
        this.multiplyMode = false;
        this.divideMode = false;
        this.addMode = false;
        this.subtractMode = false;
        this.$state = $state;
        this.$scope = $scope;
        this.dataService = dataService;
        this.dataService.currentMode = 'setup';
    }

    submit(){
        const modes = {
            '*': this.multiplyMode,
            '/': this.divideMode,
            '+': this.addMode,
            '-': this.subtractMode
        };
        this.$state.go('app.test');
        this.dataService.modes = modes;

    }
}

export default [
    '$state',
    '$scope',
    'dataService',
    SetupSettingsStateController
];

import './setup-settings-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import SetupSettingsStateController from './setup-settings-state-controller';
import setupSettingsRouteConfig from './setup-settings-route';

const dependencies = [
    'ui.router'
];

export default angular
    .module('setup-settings-state-component', dependencies)
    .controller('SetupSettingsStateController', SetupSettingsStateController)
    .config(setupSettingsRouteConfig);

# Getting started
1. To get started you'll need to install JSPM, Gulp and Protractor CLI tools globally: ``npm install -g jspm gulp protractor``

2. Run the command ``npm run setup`` to install all dependencies

3. Run the application by the command `gulp serve`